import pyspark
from pyspark import SparkContext
import cv2
import numpy as np 
import scipy as sp
import struct
from helper_functions import *
from constants import *

##INPUT: [(1 ,img1), (2, img2), (3, img3), ...]
##OUTPUT: [((1, Y), (Yarr, H, W)), ((1, Cr), (Crarr, H, W)), ((1, Cb), (Cbarr, H, W)), 
##         ((2, Y), (Yarr, H, W)), ((2, Cr), (Crarr, H, W)), ...]
## Y = 0, Cr = 1, Cb = 2

def key_value_transformation(pair):
    newVal = convert_to_YCrCb(pair[1])
    height = newVal[0].shape[0]
    width = newVal[0].shape[1]

    adjustedY = np.array(newVal[0])
    adjustedCr = np.array(newVal[1])
    adjustedCb = np.array(newVal[2])

    return (((pair[0], 0), (adjustedY, height, width)), ((pair[0], 1), (adjustedCr, height/2, width/2)), ((pair[0], 2), (adjustedCb, height/2, width/2)))

##INPUT: [((1, Y), (Yarr, H, W)), ((1, Cr), (Crarr, H, W)), ((1, Cb), (Cbarr, H, W)), 
##         ((2, Y), (Yarr, H, W)), ((2, Cr), (Crarr, H, W)), ...]
##OUTPUT: [((1, Y), ([(0, Yarr_)], H, W)), ((1, Cr), ([(0, Crarr_)], H, W)), ((1, Cb), ([(0, Cbarr_)], H, W)), ((1, Y), ([(1, Yarr_)], H, W)), ...
##         ((2, Y), ([(1, Yarr_)], H, W)), ((2, Cr), ([(1, Crarr_)], H, W)), ...]
def sub_blocking(pair):
    color_matrix = pair[1][0]
    height = pair[1][1]
    width = pair[1][2]
    color = pair[0][1]
    sub_block_arr_total = []
    block_id = 0
    for y in range(0, height, b_size):
        for x in range(0, width, b_size):
            x_end = x + b_size
            y_end = y + b_size
            sub_block_arr = color_matrix[y:y_end,x:x_end]
            sub_block_arr_total.append(((pair[0], color), ([(block_id, sub_block_arr)], height, width)))
            block_id += 1
    return sub_block_arr_total

##I/O: [((1, Y), ([(0, Yarr_)], H, W)), ((1, Cr), ([(0, Crarr_)], H, W)), ((1, Cb), ([(0, Cbarr_)], H, W)), ((1, Y), ([(1, Yarr_)], H, W)), ...
##      ((2, Y), ([(1, Yarr_)], H, W)), ((2, Cr), ([(1, Crarr_)], H, W)), ...]
def transform(pair):
    transformed = pair[1][0][0][1]
    new_val = list(pair[1])
    transformed = transformed.astype(np.float32)
    transformed = transformed - 128.0
    
    transformed = dct_block(transformed)
    transformed = quantize_block(transformed, pair[0][1] == 0, QF, False)
    transformed = quantize_block(transformed, pair[0][1] == 0, QF, True)
    transformed = dct_block(transformed, True)
    transformed = transformed + 128.0
    transformed = np.clip(transformed, 0, 255)
    new_val[0] = [(new_val[0][0][0],transformed)]
    return(pair[0], new_val)

##INPUT: [((1, Y), ([(0, Yarr_)], H, W)), ((1, Cr), ([(0, Crarr_)], H, W)), ((1, Cb), ([(0, Cbarr_)], H, W)), ((1, Y), ([(1, Yarr_)], H, W)), ...
##         ((2, Y), ([(1, Yarr_)], H, W)), ((2, Cr), ([(1, Crarr_)], H, W)), ...]
##OUTPUT: [((1, Y), ([(0, Yarr_)], H, W)), ((1, Cr), ([sub block], H, W))...]
def recombine_blocks(accum, next):
    accum_info_list = accum[0]
    block_info_list = next[0]
    height = next[1]
    width = next[2]
    return (accum_info_list + block_info_list, height, width)

##INPUT: [((1, Y), ([(0, Yarr_)], H, W)), ((1, Cr), ([sub block], H, W))
##OUTPUT: [(1, [(Y, img)]), (1, [(Cr, img]), (1, [(Cb, img)]), ...]
def populate_image(pair):
    blocks = pair[1][0]
    height = pair[1][1]
    width = pair[1][2]
    columns = width/b_size
    full_img = np.zeros((height, width), np.float32)
    for block in blocks:
        block_index = block[0]
        block_row = block_index // columns
        block_column = block_index % columns
        y_start = block_row * b_size
        x_start = block_column * b_size
        block_arr = block[1]
        for y in range(b_size):
            for x in range(b_size):
                val = block_arr[y][x]
                full_img[y + y_start][x + x_start] = val
    if pair[0][0][1]:
        full_img = resize_image(full_img, width * 2, height * 2)
    return (pair[0][0][0], [(pair[0][0][1], full_img)])

##INPUT: [(1, [(Y, img)]), (1, [(Cr, img]), (1, [(Cb, img)]), ...]
##OUTPUT: [(1, [(Y, img), (Cr, img), (Cb, img)], ...]
def recombine_colors(accum, next):
    final = accum + next
    return accum + next

##INPUT: [(1, [(Y, img), (Cr, img), (Cb, img)], ...]
#OUTPUT: [(1, img), (2, img), ...]
def stack_colors(pair):
    image_lists = pair[1]
    if image_lists[0][0] > image_lists[1][0]:
        temp = image_lists[0]
        image_lists[0] = image_lists[1]
        image_lists[1] = temp
    if image_lists[1][0] > image_lists[2][0]:
        temp = image_lists[1]
        image_lists[1] = image_lists[2]
        image_lists[2] = temp
    if image_lists[0][0] > image_lists[1][0]:
        temp = image_lists[0]
        image_lists[0] = image_lists[1]
        image_lists[1] = temp
    stacked_array = np.dstack((image_lists[0][1], image_lists[1][1], image_lists[2][1]))
    return (pair[0], stacked_array)

def convert_to_rgb(pair):
    converted_array = pair[1].astype(np.uint8)
    return (pair[0], to_rgb(converted_array))

### WRITE ALL HELPER FUNCTIONS ABOVE THIS LINE ###

def generate_Y_cb_cr_matrices(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    return rdd.flatMap(key_value_transformation)

def generate_sub_blocks(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    return rdd.flatMap(sub_blocking)

def apply_transformations(rdd):
    """
    THIS FUNCTION MUST RETURN AN RDD
    """
    return rdd.map(transform)

def combine_sub_blocks(rdd):
    """
    Given an rdd of subblocks from many different images, combine them together to reform the images.
    Should your rdd should contain values that are np arrays of size (height, width).

    THIS FUNCTION MUST RETURN AN RDD
    """
    rdd = rdd.reduceByKey(recombine_blocks)
    rdd = rdd.map(populate_image)
    rdd = rdd.reduceByKey(recombine_colors)
    rdd = rdd.map(stack_colors)
    rdd = rdd.map(convert_to_rgb)
    return rdd

def run(images):
    """
    THIS FUNCTION MUST RETURN AN RDD

    Returns an RDD where all the images will be proccessed once the RDD is aggregated.
    The format returned in the RDD should be (image_id, image_matrix) where image_matrix 
    is an np array of size (height, width, 3).
    """
    sc = SparkContext()
    rdd = sc.parallelize(images, 16) \
        .map(truncate).repartition(16)
    rdd = generate_Y_cb_cr_matrices(rdd)
    rdd = generate_sub_blocks(rdd)
    rdd = apply_transformations(rdd)
    rdd = combine_sub_blocks(rdd)

    ### BEGIN SOLUTION HERE ###

    # Add any other necessary functions you would like to perform on the rdd here
    # Feel free to write as many helper functions as necessary
    return  rdd
